Source: libxtst
Section: x11
Priority: optional
Maintainer: Debian X Strike Force <debian-x@lists.debian.org>
Build-Depends:
 debhelper-compat (= 13),
 libx11-dev,
 libxext-dev,
 libxi-dev,
 x11proto-dev,
 pkgconf,
 xutils-dev,
 quilt,
Build-Depends-Indep:
 xmlto,
 xorg-sgml-doctools,
 w3m,
Standards-Version: 4.7.0
Vcs-Git: https://salsa.debian.org/xorg-team/lib/libxtst.git
Vcs-Browser: https://salsa.debian.org/xorg-team/lib/libxtst

Package: libxtst6
Section: libs
Architecture: any
Multi-Arch: same
Pre-Depends: ${misc:Pre-Depends}
Depends: ${shlibs:Depends}, ${misc:Depends}, x11-common
Description: X11 Testing -- Record extension library
 libXtst provides an X Window System client interface to the Record
 extension to the X protocol.
 .
 The Record extension allows X clients to synthesise input events, which
 is useful for automated testing.
 .
 More information about X.Org can be found at:
 <URL:https://www.X.org>
 .
 This module can be found at
 git://anongit.freedesktop.org/git/xorg/lib/libXtst

Package: libxtst6-udeb
Package-Type: udeb
Section: debian-installer
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: X11 Testing -- Record extension library
 This is a udeb, or a microdeb, for the debian-installer.

Package: libxtst-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Depends:
 ${shlibs:Depends},
 ${misc:Depends},
 libxtst6 (= ${binary:Version}),
 libx11-dev,
 libxext-dev,
 libxi-dev,
 x11proto-dev,
Description: X11 Record extension library (development headers)
 libXtst provides an X Window System client interface to the Record
 extension to the X protocol.
 .
 The Record extension allows X clients to synthesise input events, which
 is useful for automated testing.
 .
 This package contains the development headers for the library found in
 libxtst6.  Non-developers likely have little use for this package.
 .
 More information about X.Org can be found at:
 <URL:https://www.X.org>
 .
 This module can be found at
 git://anongit.freedesktop.org/git/xorg/lib/libXtst

Package: libxtst-doc
Section: doc
Architecture: all
Multi-Arch: foreign
Depends:
 ${misc:Depends},
Replaces:
 libxtst-dev (<< 2:1.2.1),
Breaks:
 libxtst-dev (<< 2:1.2.1),
Description: X11 Record extension library (documentation)
 libXtst provides an X Window System client interface to the Record
 extension to the X protocol.
 .
 The Record extension allows X clients to synthesise input events, which
 is useful for automated testing.
 .
 This package contains the API documentation for the X Record and XTEST
 extension libraries.  Non-developers likely have little use for this package.
 .
 More information about X.Org can be found at:
 <URL:https://www.X.org>
 .
 This module can be found at
 git://anongit.freedesktop.org/git/xorg/lib/libXtst
